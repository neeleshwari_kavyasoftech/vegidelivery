//
//  AppDelegate.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 10/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import UIKit
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
   
    //Variables
    var window: UIWindow?
    //userdefalt
    var defaltAutoLogin = UserDefaults.standard
    //Variables
    var dictBaseUrl = NSMutableDictionary()
    var dictUserInformations = NSMutableDictionary()
    var strNotificationToken = ""
    let gcmMessageIDKey = "gcm.message_id"
    var intCartCount = 0
    var arrVegetables = NSMutableArray()
    var arrFruits = NSMutableArray()
    var arrCartList = NSMutableArray()
    var floatTotalPrice = Float()
    
    var  addCartList = UserDefaults.standard
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //GMSServices.provideAPIKey(PredefinedConstants().googleApiKey)
        GMSPlacesClient.provideAPIKey(PredefinedConstants().googleApiKey)
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {}
    func applicationDidEnterBackground(_ application: UIApplication) {}
    func applicationWillEnterForeground(_ application: UIApplication) {}
    func applicationDidBecomeActive(_ application: UIApplication) {}
    func applicationWillTerminate(_ application: UIApplication) {}
}

