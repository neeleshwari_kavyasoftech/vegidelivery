//
//  LeftMenuView.swift
//  ResideMenuDemo
//
//  Created by Rahul Khatri on 24/07/17.
//  Copyright © 2017 KavyaSoftech. All rights reserved.
//

import UIKit

class MenuVendor: UIViewController, UITableViewDelegate, UITableViewDataSource ,UIGestureRecognizerDelegate {
    
    
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblMobileNumber: UILabel!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var viewButton: UIView!
    @IBOutlet var btnLogout: Button!
    @IBOutlet var viewTop: UIView!
    @IBOutlet var imgEditProfile: UIImageView!
    @IBOutlet var viewBgTop: UIView!
    
    //logout
    
    //MARK:- VARIABLES
    var menuViewWidth: CGFloat!
    var arrMenu = NSMutableArray()
    var selectindex:Int = 0
    let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuItems()
        let screenWidth = UIScreen.main.bounds.width
        menuViewWidth = (screenWidth / 2) + 72
        viewBgTop.frame.size.width = menuViewWidth + 10
        viewTop.frame.size.width = viewBgTop.frame.size.width
        
        viewButton.frame.size.width = menuViewWidth
        btnLogout.frame.size.width = viewButton.frame.size.width - 10
        btnLogout.frame.origin.x = 8
    }
    //MARK:- Fuctions
    
    
    
    func menuItems() {
        arrMenu = NSMutableArray(array: [["title": "Home"],["title": "My Orders"],["title": "Cart"],["title": "Address Book"],["title": "Reward"],["title": "Notification"],["title": "Contact Us"],["title": "App Info"],["title": "Share App"]])
    }

    func userInfoDisplay(){
        let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
        
        lblUserName.text = "\(string(dictInfo, "first_name")) \(string(dictInfo, "last_name"))"
         lblMobileNumber.text = string(dictInfo, "contact_number")
   
        //product image
        let UserImage_URLString = WebServices().WEB_URL
        let strUserImg = string(dictInfo, "profile_picture")
        let imageUrl = UserImage_URLString + strUserImg
        
        if strUserImg.isEmpty {
            imgProfile.image = UIImage(named: "vegi_default")
        } else {
            let url =  NSURL(string: imageUrl)
            // cell.imgProduct.af_setImage(withURL: url as URL!)
            imgProfile.sd_setImage(with: url as URL?, placeholderImage: UIImage(named: imageUrl), options: SDWebImageOptions.retryFailed)
        }
    
    }

    //MARK:- Button Actions
    //Mark:-SIGN OUT Action====================================================================================
    
    @IBAction func actionSignOut(_ sender: Any){
        /*
        let attributedString = NSAttributedString(string: "Confirmation", attributes: [
            NSFontAttributeName : UIFont.systemFont(ofSize: 15), //your font here
            NSForegroundColorAttributeName : appColor.blueColor//UIColor.blue
            ])
        let alert = UIAlertController(title: "", message: "Are you sure, you Want to logout?\n",  preferredStyle: .alert)
        
        alert.setValue(attributedString, forKey: "attributedTitle")
        */
        
        let alert = UIAlertController (title: "Confirmation", message: "Are you sure, you Want to logout?\n" , preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
            
            var boolPoped = false
            
            for vc in (self.navigationController?.viewControllers)! {
                
                if vc is SignInVC {
                    boolPoped = true
                    self.navigationController?.popToViewController(vc, animated: false)
                }
            }
            
            if !boolPoped {
                
                self.navigationController?.viewControllers = []
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
               // self.kAppDelegate.arrGLVehicleList = NSMutableArray()
               // self.kAppDelegate.dictUserInfo = NSMutableDictionary()
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
 
    //SIGN OUT Action====================================================================================
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //PRofile Action
    @IBAction func btnEditProfile(_ sender: Any) {
    self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavProfile"), animated: true)
        self.sideMenuViewController.hideViewController()
    }

    //MARK:- TABLEVIEW DELEGATE AND DATASOURCE.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCellVendor") as! MenuCellVendor
        
        let dict = arrMenu.object(at: indexPath.row) as! NSDictionary
        let title = string(dict, "title")
        cell.lblMenu.text = string(dict, "title")
        
        if title == "Home" {
            cell.imgMenu.image = UIImage(named: "menu_home")
            
        } else if title == "My Orders" {
            cell.imgMenu.image = UIImage(named: "menu_my_orders")
            
        } else if title == "Cart" {
            cell.imgMenu.image = UIImage(named: "menu_cart")
            
        } else if title == "Address Book" {
            cell.imgMenu.image = UIImage(named: "menu_address_book")
            
        } else if title == "Reward" {
            cell.imgMenu.image = UIImage(named: "menu_reward")
            
        } else if title == "Notification" {
            cell.imgMenu.image = UIImage(named: "menu_notification")
            
        } else if title == "Contact Us" {
            cell.imgMenu.image = UIImage(named: "menu_contact")
            
        }else if title == "App Info" {
            cell.imgMenu.image = UIImage(named: "menu_app_info")
            
        }else if title == "Share App" {
            cell.imgMenu.image = UIImage(named: "menu_share")
        }
        
        
        if indexPath.row == selectindex {
            cell.backgroundColor = UIColor(named: "appTheme")
            cell.lblMenu.textColor = UIColor.white
            
        } else {
            cell.lblMenu.textColor = UIColor.black
            cell.backgroundColor = UIColor.white
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblView{
            let dict = arrMenu.object(at: indexPath.row) as! NSDictionary
            let title = string(dict, "title")
            
            arrMenu = NSMutableArray(array: [["title": "Home"],["title": "My Orders"],["title": "Cart"],["title": "Address Book"],["title": "Reward"],["title": "Notification"],["title": "Contact Us"],["title": "App Info"],["title": "Share App"]])
            
            if title == "Home" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavHome"), animated: true)
                self.sideMenuViewController.hideViewController()
                
            } else if title == "My Orders" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavMyOrders"), animated: true)
                
                self.sideMenuViewController.hideViewController()
                
            } else if title == "Cart" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                //Http.alert("", "Under Working")
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavCart"), animated: true)
                self.sideMenuViewController.hideViewController()
                
            } else if title == "Address Book" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavAddress"), animated: true)
                self.sideMenuViewController.hideViewController()
                
            } else if title == "Reward" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavReward"), animated: true)
                // kAppDelegate.menuPush = "menubtn"
                self.sideMenuViewController.hideViewController()
                
            } else if title == "Notification" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavNotification"), animated: true)
                self.sideMenuViewController.hideViewController()
                
            } else if title == "Contact Us" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavContactUs"), animated: true)
                self.sideMenuViewController.hideViewController()
            }else if title == "App Info" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                self.sideMenuViewController.setContentViewController(self.storyboard?.instantiateViewController(withIdentifier: "NavAboutUs"), animated: true)
                self.sideMenuViewController.hideViewController()
            }else if title == "Share App" {
                
                selectindex = indexPath.row
                tblView.reloadData()
                print("share App")
                
                let text = "Download our new app to place online order of Vegetables and fruits\nVegiDelivery: www.vegidelivery.com "
                
                // set up activity view controller
                let textToShare = [ text ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                
                // exclude some activity types from the list (optional)
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
                
                // present the view controller
                self.present(activityViewController, animated: true, completion: nil)
            }
        }else{
            
        }
        
    }
    
    //MARK:-Notification work ===========================Neeleshwari****************************************
    
    let windowScreen = UIApplication.shared.keyWindow!
    
    override func viewWillAppear(_ animated: Bool) {
        self.userInfoDisplay()
    }
    
    //MARK:-Notification work ===========================Neeleshwari****************************************
    
} //class ends here....
















