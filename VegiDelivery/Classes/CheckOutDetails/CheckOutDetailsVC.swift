//
//  CheckOutDetailsVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 19/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit

class CheckOutDetailsVC: UIViewController , UITableViewDelegate , UITableViewDataSource  , UITextViewDelegate{
    
    @IBOutlet var scrlView: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var tblViewProducts: UITableView!
    
    @IBOutlet var viewTaxes: UIView!
    @IBOutlet var lblTotalAmount: UILabel!
    @IBOutlet var lblGST: UILabel!
    @IBOutlet var lblShippingCharges: UILabel!
    @IBOutlet var lblRewards: UILabel!
    @IBOutlet var lblGrandTotal: UILabel!
    @IBOutlet var viewBGInstruction: UIView!
    @IBOutlet var lblSpecialInstructions: UILabel!
    @IBOutlet var tvInstructions: UITextView!
    @IBOutlet var lblPlaceHolder: UILabel!
    @IBOutlet var btnConfirm: UIButton!
    
    @IBOutlet var tblViewAddress: UITableView!
    //Constraint
    @IBOutlet var tblProductHeightConstraint: NSLayoutConstraint!
    @IBOutlet var viewTaxesHeightConstraint: NSLayoutConstraint!
    @IBOutlet var tblViewAddressHeightConstaint: NSLayoutConstraint!
    @IBOutlet var subViewHeightConstaint: NSLayoutConstraint!
    
    //MARK:- Variables
    var arrItemList = NSMutableArray()
    var arrAddressList = NSMutableArray()
    var strPaymentMode = ""
    var intAddressSelected = -1
    var intPaymentModeSelected = -1
    var boolWsPage = Bool()
    var page = Int()
    var floatTotalAmount = Float()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavVegiDeliveryGreen(className: "CHECKOUT")
        self.menuNavigationButton()
        self.registerForKeyboardNotifications()
        
        page = 1
        self.WS_AddressList()
        self.setTaxesDetails()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.setHt()
            self.tblViewAddress.reloadData()
            self.tblViewProducts.reloadData()
        })
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        tblViewProducts.delegate = self
        tblViewProducts.dataSource = self
        
        tblViewAddress.delegate = self
        tblViewAddress.dataSource = self
        tvInstructions.delegate = self
        
        let dummyViewHeight = CGFloat(40)
        self.tblViewAddress.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tblViewAddress.bounds.size.width, height: dummyViewHeight))
        self.tblViewAddress.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
        
        btnConfirm.border(UIColor(named: "redColor"), 20, 1.0)
        // tblViewCheckOut.estimatedRowHeight = UITableViewAutomaticDimension
        //   tblViewCheckOut.rowHeight = 100
        // = PaddingLabel(8, 8, 16, 16)
        
        lblSpecialInstructions = PaddingLabel(withInsets: 5, 5, 5, 5)
        ///////////
        
        tblViewProducts.separatorStyle = .none
        tblViewAddress.separatorStyle = .none
        
        tvInstructions.delegate = self
        tblViewProducts.isScrollEnabled = false
        tblViewAddress.isScrollEnabled = false
        
        tvInstructions.tintColor = UIColor.black
        tvInstructions.tintColor = UIColor(named: "appTheme")
        
    }
    
    func setTaxesDetails() {
        
        floatTotalAmount = PredefinedConstants.appDelegate.floatTotalPrice
        
        var rewardPoints = Float()
        
        let SHIPPING_CHARGE = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "SHIPPING_CHARGE"))"
        let GST_PERCENTAGE = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "GST_PERCENTAGE"))"
        
        let PER_REWARD_POINT = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "PER_REWARD_POINT"))"
        
        let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
        
        let user_reward_points =  string(dictInfo, "user_reward_points").count
        
        rewardPoints = Float(PER_REWARD_POINT)! * Float(user_reward_points)
        
        let gstPrice :Float = Float(GST_PERCENTAGE)! * floatTotalAmount * 0.01
        
        lblTotalAmount.text = String(format: "Total: ₹ %.2f", floatTotalAmount)//"Total: ₹" + "\(floatTotalAmount)"//string(dictOrderDetails, "total")
        lblGST.text = "Gst(+\(GST_PERCENTAGE)%): ₹ " + String(format: "%.2f", gstPrice) //"Gst(+\(GST_PERCENTAGE)%): ₹" + "\(gstPrice)"
        lblShippingCharges.text =  String(format: "Shipping Charge(+): ₹ %.2f", Float(SHIPPING_CHARGE)!)//"Shipping Charge(+): ₹" + "\(SHIPPING_CHARGE)"
        lblRewards.text =  String(format: "Reward(-): ₹ %.2f", rewardPoints)//"Reward(-): ₹" + "\(rewardPoints)"
        
        let finalTotal = floatTotalAmount +  Float(SHIPPING_CHARGE)! + gstPrice - Float(rewardPoints)
       // print("finalTotal---->>\(finalTotal)")
        
        lblGrandTotal.text = String(format: "Grand Total: ₹ %.2f", finalTotal)//"Grand Total: ₹" + "\(finalTotal)"
        
    }
    
    func setHt()  {
        
        let count1 = Double(arrItemList.count)//5.0 //arrItemList
        let cellHt1 = 117.0
        
        let tblHeight1 = Double(count1*cellHt1)
        
        let count2 = Double(arrAddressList.count)//5.0 //address
        let cellHt2 = 65.0
        
        let count3 = 1.0 ///shopping mode COD
        
        let tblHeight2 = Double(count2*cellHt2)
        let tblHeight3 = Double(count3*cellHt2)
        
        tblProductHeightConstraint.constant = CGFloat(tblHeight1)
        tblViewAddressHeightConstaint.constant = CGFloat(tblHeight2 + tblHeight3 + 100)
        scrlView.contentSize.height = subViewHeightConstaint.constant + 20
        
        subViewHeightConstaint.constant = tblProductHeightConstraint.constant + viewTaxesHeightConstraint.constant + tblViewAddressHeightConstaint.constant + lblSpecialInstructions.frame.size.height + tvInstructions.frame.size.height + btnConfirm.frame.size.height + 50
        viewBGInstruction.addBottomBorder(color: UIColor.black, height: 1)
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "ic_arrow_back"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionMenuButton()  {
        //self.sideMenuViewController.presentLeftMenuViewController()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
        scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scrlView.contentInset = UIEdgeInsets.zero
    }
    
    //MARK:- Button Actions
    
    @IBAction func btnConfirmAction(_ sender: Any) {
        self.hideKeyboard()
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            //  self.ws_SignIn()
            self.setJsonArrayData()
        }
        
    }
    
    //MARK:- Table View delegates and datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == tblViewProducts {
            
            return 1
        }else if tableView == tblViewAddress {
            
            return 2
        }else{
            
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblViewProducts {
            
            return arrItemList.count
        }else if tableView == tblViewAddress {
            
            if section == 0{
                
                return arrAddressList.count
            }else{
                return 1
            }
        }else{
            
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblViewProducts {
            
            let cell = tblViewProducts.dequeueReusableCell(withIdentifier: "ProductsCell") as! ProductsCell
            cell.selectionStyle = .none
            
            let dict = arrItemList.object(at: indexPath.row) as! NSDictionary
            
            cell.lblName.text = string(dict, "title")
            cell.lblPrice.text = "₹\(string(dict, "price"))"//"Price: ₹4545.20"
            
            cell.lblQuantity.text = string(dict, "quantity")
            let strOption = string(dict, "option")
            let dictOption = convertToDictionary(text: strOption) as! NSDictionary
            cell.lblWeight.text = string(dictOption, "weight")
            
            //product image
            let UserImage_URLString = WebServices().WEB_URL
            let strUserImg = string(dict, "image")
            let imageUrl = UserImage_URLString + strUserImg
            
            if strUserImg.isEmpty {
                cell.imgProduct.image = UIImage(named: "vegi_default")
            } else {
                let url =  NSURL(string: imageUrl)
                // cell.imgProduct.af_setImage(withURL: url as URL!)
                cell.imgProduct.sd_setImage(with: url as URL?, placeholderImage: UIImage(named: imageUrl), options: SDWebImageOptions.retryFailed)
            }
            
            return cell
        }else{
            let cell = tblViewAddress.dequeueReusableCell(withIdentifier: "CheckOutAddressCell") as! CheckOutAddressCell
            
            cell.btnChecked.layer.cornerRadius =   cell.btnChecked.frame.size.height/2
            cell.btnChecked.clipsToBounds = true
            cell.btnChecked.layer.borderColor = UIColor.red.cgColor
            cell.btnChecked.layer.borderWidth = 0
            
            cell.selectionStyle = .none
            
            if indexPath.section == 0{
                
                let dictData = arrAddressList.object(at: indexPath.row) as! NSDictionary
                
                cell.lblAddress.text = string(dictData, "address")
                
                if intAddressSelected == indexPath.row{
                    
                    cell.btnChecked.setImage(UIImage(named: "check_circle"), for: .normal)
                }else{
                    cell.btnChecked.setImage(UIImage(named: "uncheck_circle"), for: .normal)
                    
                }
                if indexPath.row == (arrAddressList.count - 1) {
                    if !boolWsPage {
                        boolWsPage = true
                        if arrAddressList.count % 50 == 0 {
                            page += 1
                            self.WS_AddressList()
                        }
                    }
                }
                
            }else{
                intPaymentModeSelected = 0
                
                if intPaymentModeSelected == indexPath.row{
                    
                    cell.btnChecked.setImage(UIImage(named: "check_circle"), for: .normal)
                }else{
                    cell.btnChecked.setImage(UIImage(named: "uncheck_circle"), for: .normal)
                    
                }
                cell.lblAddress.text = "COD"
            }
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblViewProducts {
        }else{
            if indexPath.section == 0{
                intAddressSelected = indexPath.row
                tblViewAddress.reloadData()
            }else{
                intPaymentModeSelected = indexPath.row
                tblViewAddress.reloadData()
            }
        }
    }
    
    //Mark:-tableView viewForHeaderInSection
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var viewHeader = UIView()
        
        if tableView == tblViewAddress {
            
            var ht = CGFloat()
            var lblTitleY = CGFloat()
            
            if section == 0{
                ht = 40
                lblTitleY = 0
            }else{
                ht = 50
                lblTitleY = 10
            }
            
            viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: self.tblViewAddress.frame.size.width, height: ht))
            
            let  viewBG = UIView(frame: CGRect(x: 0, y: lblTitleY, width: self.tblViewAddress.frame.size.width, height: 40))
            
            let lblSectionTitle = UILabel(frame: CGRect(x: 5, y: 0, width: self.tblViewAddress.frame.size.width - 50, height: 40))
            
            lblSectionTitle.font =  UIFont.systemFont(ofSize: 15, weight: .semibold)
            lblSectionTitle.textColor = UIColor.black
            lblSectionTitle.numberOfLines = 1
            
            viewBG.addBottomBorder(color: UIColor.black, height: 1)
            
            let btnAddNew = UIButton(frame: CGRect(x: self.tblViewAddress.frame.size.width - 40, y: 0, width: 40, height: 40))
            btnAddNew.setImage(UIImage(named: "add_green"), for: .normal)
            btnAddNew.setTitle("", for: .normal)
            btnAddNew.addTarget(self, action: #selector(actionAddNew), for: .touchUpInside)
            
            if section == 0 {
                lblSectionTitle.text = "Shipping Address"
                
                viewBG.addSubview(lblSectionTitle)
                viewBG.addSubview(btnAddNew)
                
            }else if section == 1 {
                lblSectionTitle.text = "Payment Methods"//"\
                viewBG.addSubview(lblSectionTitle)
            }else {
                lblSectionTitle.text = ""
            }
            viewHeader.addSubview(viewBG)
            viewBG.backgroundColor = UIColor.white
            lblSectionTitle.backgroundColor = UIColor.clear
            btnAddNew.backgroundColor = UIColor.clear
            viewHeader.backgroundColor = UIColor.clear
        }
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblViewProducts {
            
            return 100
        }else{
            
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if tableView == tblViewAddress{
            
            if section == 0{
                return 40
            }else{
                return 50
            }
        }else{
            return 0
        }
    }
    
    @objc func actionAddNew() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- TEXTVIEW_DELEGATE *-*-*-*-*-*-*-*-*
    func textViewDidBeginEditing(_ textView: UITextView) {
        lblPlaceHolder.isHidden = true
        /*
         if UIScreen.main.bounds.size.height < 667{
         if (textView.frame.origin.y >= 180) {
         scrlView.setContentOffset(CGPoint(x: 0, y: CGFloat(Int(textView.frame.origin.y - 50))) , animated: true)
         }
         }
         */
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if (text == "\n") {
            if tvInstructions.text == "" {
                lblPlaceHolder.isHidden = false
            } else {
                lblPlaceHolder.isHidden = true
            }
            
            tvInstructions.resignFirstResponder()
            //  scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            
            return false
        }
        var maxCharacter = Int()
        maxCharacter = 250
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return (numberOfChars < maxCharacter)
    }
    //MARK:- Check Validations
    func checkValidation() -> String? {
        
        if intAddressSelected == -1 {
            return AlertMSG.selectAddress
            
        }
        /*
         else if strPaymentMode.count == 0  {
         return AlertMSG.selectPaymentMethod
         }*/
        
        return nil
    }
    
    //MARK:- KEYBOARD NOTIFICATION METHODS
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scrlView.contentInset = UIEdgeInsets.zero
        } else {
            scrlView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        scrlView.scrollIndicatorInsets = scrlView.contentInset
    }
    //
    //MARK:- ------------------ WS_AddressList ----------------------//
    
    func WS_AddressList() {
        
        let params = NSMutableDictionary()
        params["page"] = page
        Http.instance().json(WebServices().WS_CustomerAddressList, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    self.arrAddressList = NSMutableArray()
                    if let arr = json?.object(forKey: "result") as? NSArray {
                        // self.arrList  = arr.mutableCopy() as! NSMutableArray
                        print("arr>>>\(arr)")
                        
                        if self.boolWsPage {
                            self.arrAddressList.addObjects(from: arr as [AnyObject])
                            self.boolWsPage = false
                        } else {
                            self.arrAddressList = (arr.mutableCopy() as? NSMutableArray)!
                        }
                        self.tblViewAddress.reloadData()
                    }
                    
                } else {
                    //  Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    //MARK:- ------------------ WS_ORDER_CREATE ----------------------//
    
    //var dictJson = NSMutableArray()
    var dictJson =  NSMutableDictionary()
    let arrItem_details = NSMutableArray()
    
    func setJsonArrayData() {
        
        // dictJson
        //  dictJson.add(["option": dictOption,"product_id": id ,"quantity": cart_quantity ,"price": price])
        var total  = Float()
        
        for i in 0..<arrItemList.count {
            let dictItem = arrItemList.object(at: i) as! NSDictionary
            let product_id = string(dictItem, "product_id")
            let name = string(dictItem, "title")
            let offer_id = string(dictItem, "offer_id")
            let offer = string(dictItem,"offer")
            let offer_type = string(dictItem,"offer_type")
            let quantity = string(dictItem,"quantity")
            let price = string(dictItem, "price")
            let offer_price = string(dictItem,"offer_price")
            let subtotal = string(dictItem,"subtotal")
            let reward = string(dictItem,"reward")
            let strOption = string(dictItem, "option")
            let dictOption = convertToDictionary(text: strOption)! as NSDictionary
            let weight = string(dictOption, "weight")
            
            floatTotalAmount =  PredefinedConstants.appDelegate.floatTotalPrice
            
            let newDictOption = NSMutableDictionary()
            newDictOption.setValue(weight, forKey: "weight")
            
            arrItem_details.add(["product_id": product_id,"name": name ,"offer_id": offer_id ,"offer": offer ,"offer_type": offer_type ,"quantity": quantity ,"price": price ,"offer_price": offer_price ,"subtotal": subtotal ,"reward": reward ,"option": newDictOption ])
        }
        dictJson.setValue(arrItem_details.convertToString(), forKey: "item_details")
        
        //arrShipping_details
        
        let dictAddress = arrAddressList.object(at:intAddressSelected )  as! NSDictionary
        
        let shipping_name = string(dictAddress, "name")
        let shipping_mobile = string(dictAddress, "mobile_number")
        let shipping_city = string(dictAddress, "city_name")
        let shipping_address = string(dictAddress, "address")
        let shipping_state = string(dictAddress, "state_name")
        let shipping_country = string(dictAddress, "country_name")
        let shipping_pincode = string(dictAddress, "pincode")
        let shipping_latitude = string(dictAddress, "latitude")
        let shipping_longitude = string(dictAddress, "longitude")
        
        let dictShipping_details = NSMutableDictionary()
        
        dictShipping_details.setValue(shipping_name, forKey: "shipping_name")
        dictShipping_details.setValue(shipping_mobile, forKey: "shipping_mobile")
        dictShipping_details.setValue(shipping_city, forKey: "shipping_city")
        dictShipping_details.setValue(shipping_address, forKey: "shipping_address")
        dictShipping_details.setValue(shipping_state, forKey: "shipping_state")
        dictShipping_details.setValue(shipping_country, forKey: "shipping_country")
        dictShipping_details.setValue(shipping_pincode, forKey: "shipping_pincode")
        dictShipping_details.setValue(shipping_latitude, forKey: "shipping_latitude")
        dictShipping_details.setValue(shipping_longitude, forKey: "shipping_longitude")
        
        dictJson.setValue(dictShipping_details, forKey: "shipping_details")
        //other
        let comment = tvInstructions.text!
        //21463d51
        
        var rewardPoints = Float()
        
        let SHIPPING_CHARGE = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "SHIPPING_CHARGE"))"
        let GST_PERCENTAGE = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "GST_PERCENTAGE"))"
        
        let PER_REWARD_POINT = "\(string(PredefinedConstants.appDelegate.dictBaseUrl, "PER_REWARD_POINT"))"
        
        let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
        
        let user_reward_points =  string(dictInfo, "user_reward_points").count
        
        rewardPoints = Float(PER_REWARD_POINT)! * Float(user_reward_points)
        
        dictJson.setValue("", forKey: "txnid")
        dictJson.setValue("cod", forKey: "shipping_method")
        dictJson.setValue("", forKey: "paid")
        dictJson.setValue(comment, forKey: "comment")
        dictJson.setValue("", forKey: "sub_total")
        dictJson.setValue(SHIPPING_CHARGE, forKey: "shipping")
        dictJson.setValue(rewardPoints, forKey: "used_reward_points")
        dictJson.setValue("0", forKey: "get_reward_points")
        dictJson.setValue(floatTotalAmount, forKey: "total")
        dictJson.setValue("1", forKey: "order_status_id")
        dictJson.setValue(shipping_latitude, forKey: "latitude")
        dictJson.setValue(shipping_longitude, forKey: "longitude")
        dictJson.setValue(GST_PERCENTAGE, forKey: "GST_PERCENTAGE")
        self.WS_ORDER_CREATE()
    }
    
    //MARK:- Add to cart
    
    func WS_ORDER_CREATE() {
        
        let params = NSMutableDictionary()
        params["order_json"] = dictJson.convertToString()
        
        Http.instance().json(WebServices().WS_ORDER_CREATE, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // self.arrOrderList = NSMutableArray()
                    Http.alert("", string(json! , "message"))
                    
                    let vc  = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    
    
}//Class Ends Here ===========================.....Neeleshwari

class ProductsCell :UITableViewCell{
    
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblWeight: UILabel!
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lblQuantity: UILabel!
    
    //Variables
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

class CheckOutAddressCell :UITableViewCell{
    
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var btnChecked: UIButton!
    
    //Variables
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func btnCheckedAction(_ sender: Any) {
        
        
    }
    
}

class PaddingLabel: UILabel {
    
    var topInset: CGFloat
    var bottomInset: CGFloat
    var leftInset: CGFloat
    var rightInset: CGFloat
    
    required init(withInsets top: CGFloat, _ bottom: CGFloat,_ left: CGFloat,_ right: CGFloat) {
        self.topInset = top
        self.bottomInset = bottom
        self.leftInset = left
        self.rightInset = right
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}



