//
//  AddressListVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 16/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit

class AddressListVC: UIViewController , UITableViewDelegate , UITableViewDataSource ,AddressListCellDelegate  {
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet var lblNoRecords: UILabel!
    
    //MARK:- Variables
    var arrAddressList = NSMutableArray()
    var boolWsPage = Bool()
    var page = Int()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavVegiDeliveryGreen(className: "ADDRESS LIST")
        self.menuNavigationButton()
        self.addNewAddressButton()
        page = 1
        self.WS_AddressList()
        lblNoRecords.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        /*
         if arrAddress.count == 0 {
         lblNoRecords.isHidden = false
         }else{
         lblNoRecords.isHidden = true
         }
         */
    }
    //MARK:- Fuctions
    func displayAndDelegates()  {
        tblView.delegate = self
        tblView.dataSource = self
        
        let dummyViewHeight = CGFloat(40)
        self.tblView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.bounds.size.width, height: dummyViewHeight))
        self.tblView.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "menu_white"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
        // _ = self.navigationController?.popViewController(animated: true)
    }
    func addNewAddressButton() {
        let buttonAdd = UIButton.init(type: .custom)
        buttonAdd.setImage(UIImage.init(named: "ic_add_address"), for: UIControlState.normal)
        buttonAdd.addTarget(self, action:#selector(btnAddNewAddressAction), for:.touchUpInside)
        buttonAdd.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        let barButton = UIBarButtonItem.init(customView: buttonAdd)
        buttonAdd.backgroundColor = UIColor.clear
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func btnAddNewAddressAction() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Table View delegates and datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrAddressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "AddressListCell") as! AddressListCell
        
        let dictData = arrAddressList.object(at: indexPath.row) as! NSDictionary
        
        cell.lblName.text = string(dictData, "name")
        cell.lblMobileNumber.text = string(dictData, "mobile_number")
        cell.lblAddress.text = string(dictData, "address")
        
        cell.imgAddress.layer.cornerRadius =   cell.imgAddress.frame.size.height/2
        cell.imgAddress.clipsToBounds = true
        cell.imgAddress.layer.borderColor = UIColor.red.cgColor
        cell.imgAddress.layer.borderWidth = 0
        
        cell.delegateAddressListCell = self
        cell.intIndex = indexPath.row
        
        if indexPath.row == (arrAddressList.count - 1) {
            if !boolWsPage {
                boolWsPage = true
                if arrAddressList.count % 50 == 0 {
                    page += 1
                    self.WS_AddressList()
                }
            }
        }
        return cell
    }
    
    //MARK:- AddressListCell Button Actions
    
    func btnEdit(intIndex: Int){
        let dictData = arrAddressList.object(at: intIndex) as! NSDictionary
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        vc.dictAddress = dictData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func btnDelete(intIndex: Int){
        self.WS_DeleteAddress(intIndex: intIndex)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //tableView viewForHeaderInSection
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.frame.size.width, height: 40))
        
        let lblSectionTitle = UILabel(frame: CGRect(x: 8, y: 0, width: self.tblView.frame.size.width - 8, height: 40))
        lblSectionTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        lblSectionTitle.numberOfLines = 1
        lblSectionTitle.backgroundColor = UIColor.clear
        lblSectionTitle.text = "ADDRESS"
        lblSectionTitle.textColor = UIColor(named:"grayTextColor")//UIColor.black
        viewHeader.addSubview(lblSectionTitle)
        viewHeader.backgroundColor = UIColor.clear
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if arrAddressList.count == 0{
            return 0
        }else{
            return 40
        }
    }
    
    //MARK:- ------------------ WS_AddressList ----------------------//
    func WS_AddressList() {
        
        let params = NSMutableDictionary()
        params["page"] = page
        Http.instance().json(WebServices().WS_CustomerAddressList, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    self.arrAddressList = NSMutableArray()
                    if let arr = json?.object(forKey: "result") as? NSArray {
                        if self.boolWsPage {
                            self.arrAddressList.addObjects(from: arr as [AnyObject])
                            self.boolWsPage = false
                        } else {
                            self.arrAddressList = (arr.mutableCopy() as? NSMutableArray)!
                        }
                        if self.arrAddressList.count == 0 {
                            self.lblNoRecords.isHidden  = false
                        }else{
                            self.lblNoRecords.isHidden  = true
                        }
                        self.tblView.reloadData()
                    }
                    
                } else {
                    //  Http.alert("", string(json! , "message"))
                    self.lblNoRecords.isHidden  = false
                    self.lblNoRecords.text = string(json! , "message")
                    self.arrAddressList = NSMutableArray()
                    self.tblView.reloadData()
                }
            }
        }
    }
    
    //MARK:- ------------------ Delete Address ----------------------//
    func WS_DeleteAddress(intIndex:Int) {
        let dictAddress = arrAddressList.object(at: intIndex) as! NSDictionary
        let params = NSMutableDictionary()
        params["address_id"] = string(dictAddress, "address_id")
        Http.instance().json(WebServices().WS_DeleteAddress, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    Http.alert("", string(json! , "message"))
                    self.arrAddressList.removeObject(at: intIndex)
                    self.WS_AddressList()
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
}//Class Ends Here ===========================.....Neeleshwari

public protocol AddressListCellDelegate {
    
    func btnEdit(intIndex: Int)
    func btnDelete(intIndex: Int)
    
}

class AddressListCell :UITableViewCell{
    
    @IBOutlet var imgAddress: UIImageView!
    @IBOutlet var viewBG: View!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblMobileNumber: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnDelete: UIButton!
    
    //Variables
    var  intIndex = Int()
    var delegateAddressListCell: AddressListCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK:- Button Actions
    @IBAction func btnEditAction(_ sender: Any) {
        delegateAddressListCell.btnEdit(intIndex: intIndex)
        
    }
    
    @IBAction func btnDeleteAction(_ sender: Any) {
        delegateAddressListCell.btnDelete(intIndex: intIndex)
    }
    
    
}





