//
//  ProfileVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 17/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit

class ProfileVC: UIViewController , UITextFieldDelegate , UIImagePickerControllerDelegate,  UINavigationControllerDelegate{
    
    @IBOutlet var scrlView: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var imgBG: ImageView!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var imgCamera: UIImageView!
    @IBOutlet var btnProfile: UIButton!
    @IBOutlet var tfFirstName: SkyFloatingLabelTextField!
    @IBOutlet var tfLastName: SkyFloatingLabelTextField!
    @IBOutlet var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet var tfContactNumber: SkyFloatingLabelTextField!
    @IBOutlet var tfDateOfBirth: SkyFloatingLabelTextField!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var btnCalendar: UIButton!
    @IBOutlet var viewDatePicker: View!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var btnDonePicker: UIButton!
    @IBOutlet var btnCancelPicker: UIButton!
    
    
    //MARK:- Variables
    var picker:UIImagePickerController? = UIImagePickerController()
    var imgProfilePicture:UIImage? = nil
    var boolProfilePicture = false
    var lastClass = ""
    
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
        self.addDoneButtonOnKeyboard()
        self.hideKeyboard()
        removePickerView(bottomView: viewDatePicker)
        viewDatePicker.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavVegiDeliveryGreen(className: "MY ACCOUNT")
        self.registerForKeyboardNotifications()
        self.userInfoDisplay()
       self.lastClassBack()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
    }
    
    func lastClassBack()  {
        
        if lastClass == "cart"{
            self.backNavigationButton()
        }else{
            self.menuNavigationButton()
        }
    }
    
    
    func backNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "ic_arrow_back"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionBackButton()  {
        //self.sideMenuViewController.presentLeftMenuViewController()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func userInfoDisplay(){
           let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
        
        tfFirstName.text = string(dictInfo, "first_name")
        tfLastName.text = string(dictInfo, "last_name")
        tfEmail.text = string(dictInfo, "email")
        tfContactNumber.text = string(dictInfo, "contact_number")
        tfDateOfBirth.text = string(dictInfo, "dob")
        
        
        //product image
        let UserImage_URLString = WebServices().WEB_URL
        let strUserImg = string(dictInfo, "profile_picture")
        let imageUrl = UserImage_URLString + strUserImg
        
        if strUserImg.isEmpty {
            imgProfile.image = UIImage(named: "vegi_default")
            
        } else {
            let url =  NSURL(string: imageUrl)
            // cell.imgProduct.af_setImage(withURL: url as URL!)
            imgProfile.sd_setImage(with: url as URL?, placeholderImage: UIImage(named: imageUrl), options: SDWebImageOptions.retryFailed)
        }
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        
        tfFirstName.delegate = self
        tfLastName.delegate = self
        btnSubmit.border(UIColor.clear, 22, 0)
        datePicker.maximumDate = NSDate() as Date
        
        let userIconName = UIImage(named: "name")
        setPaddingWithRightImage(image: userIconName!, textField: tfFirstName)
        setPaddingWithRightImage(image: userIconName!, textField: tfLastName)
        
        let userIconEmail = UIImage(named: "email_id")
        setPaddingWithRightImage(image: userIconEmail!, textField: tfEmail)
        
        let userIconContact = UIImage(named: "mobile")
        setPaddingWithRightImage(image: userIconContact!, textField: tfContactNumber)
        
        let userIconDate = UIImage(named: "ic_calender")
        setPaddingWithRightImage(image: userIconDate!, textField: tfDateOfBirth)
        
        // tfDateOfBirth.addTarget(self, action: #selector(tfSelectDateAction), for: UIControlEvents.touchDown)
        
        // var view = new UIView(new CGRect(View.Frame.Left, View.Frame.Height - 200, View.Frame.Right, 0))
        viewDatePicker.backgroundColor = UIColor.clear
        
        viewDatePicker.isHidden = true
        
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "menu_white"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
        //_ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Button Actions
    @IBAction func btnSubmitAction(_ sender: Any) {
        self.hideKeyboard()
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            //  self.ws_SignIn()
            WS_UpdateProfile()
        }
    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        self.getGallery()
    }
    
    @IBAction func btnCalendar(_ sender: Any) {
        //  tfDateOfBirth.becomeFirstResponder()
        // self.hideKeyboard()
        datePicker.maximumDate = NSDate() as Date
        addPickerView(bottomView: viewDatePicker)
        viewDatePicker.isHidden = false
    }
    
    @IBAction func datePickerAction(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        //  tfDateOfBirth.text = dateFormatter.string(from: datePicker.date)
    }
    
    //MARK:- Picker button actions
    @IBAction func btnCancelPicker(_ sender: Any) {
        removePickerView(bottomView: viewDatePicker)
        viewDatePicker.isHidden = true
    }
    @IBAction func btnDonePicker(_ sender: Any) {
        removePickerView(bottomView: viewDatePicker)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        tfDateOfBirth.text = dateFormatter.string(from: datePicker.date)
        self.view.endEditing(true)
        viewDatePicker.isHidden = true
    }
    
    //MARK:- Check Validations
    func checkValidation() -> String? {
        
        if imgProfilePicture == nil {
            return AlertMSG.blankDP
            
        }else if tfFirstName.text?.count == 0  {
            return AlertMSG.blankFirstName
            
        }else if  tfLastName.text?.count == 0  {
            return AlertMSG.blankLastName
            
        }else if (tfDateOfBirth.text?.count)! == 0 {
            return AlertMSG.blankDate
        }
        
        return nil
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
        scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scrlView.contentInset = UIEdgeInsets.zero
    }
    
    //MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        removePickerView(bottomView: datePicker)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfFirstName {
            tfLastName.becomeFirstResponder()
        }else if textField == tfLastName {
            tfLastName.resignFirstResponder()
            scrlView.contentInset = UIEdgeInsets.zero
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        if textField == tfFirstName {
            return (length > 40) ? false : true
        }else if textField == tfLastName {
            return (length > 40) ? false : true
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrlView.contentSize = CGSize(width: view.frame.size.width, height: btnSubmit.frame.origin.y + btnSubmit.frame.size.height + 10)
    }
    
    ///MARK:- AddDoneButtonOnKeyboard
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        doneToolbar.barTintColor = UIColor(named:"appTheme") //appColor.appThemeGreenColor
        doneToolbar.tintColor = UIColor.white
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let next: UIBarButtonItem = UIBarButtonItem(title: "NEXT", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.nextOfDoneTool))
        let done: UIBarButtonItem = UIBarButtonItem(title: "DONE", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.hideKeyboard))
        
        var items:[UIBarButtonItem] = []
        items.append(next)
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.tfContactNumber.inputAccessoryView = doneToolbar
    }
    
    @objc func nextOfDoneTool() {
        tfEmail.becomeFirstResponder()
    }
    
    func done() {
        tfContactNumber.resignFirstResponder()
        scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    //MARK:- KEYBOARD NOTIFICATION METHODS
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scrlView.contentInset = UIEdgeInsets.zero
        } else {
            scrlView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        scrlView.scrollIndicatorInsets = scrlView.contentInset
    }
    //MARK:- CAMERA AND GALLARY FUNCTION
    
    func getGallery(){
        self.hideKeyboard()
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func photoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //MARK : CAMERAVIEW DELEGATES
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let img:UIImage? = info[UIImagePickerControllerEditedImage] as? UIImage
        
        if (img != nil) {
            imgProfile.image = img
            imgProfilePicture = img
        }
        
        if (img != nil) {
            imgProfile.image = img
            boolProfilePicture = true
            imgProfilePicture = img
        }
        picker.dismiss(animated: true, completion: nil);
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- WS_ContactUs
    
    func WS_UpdateProfile() {
        
        let params = NSMutableDictionary()
        params["first_name"] = tfFirstName.text
        params["last_name"] = tfLastName.text
        params["email"] = tfEmail.text
        
        params["contact_number"] = tfContactNumber.text
        params["dob"] = "customer"
        
        let images = NSMutableArray()
        
        if boolProfilePicture {
            if imgProfilePicture != nil {
                let md = NSMutableDictionary()
                
                md["param"] = "profile_picture"
                md["image"] = imgProfilePicture!.resize(100.0)
                images.add(md)
            }
        }
        
        Http.instance().json(WebServices().WS_EDIT_PROFILE, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken(),images) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // self.arrOrderList = NSMutableArray()
                    Http.alert("", string(json! , "message"))
                    if let result = json?.object(forKey: "result") as? NSDictionary {
                        self.updateUserInfo(dictResult:result)
                      
                        if self.lastClass == "cart"{
                           _ = self.navigationController?.popViewController(animated: true)
                        }
                    }
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }

    
    func  updateUserInfo(dictResult:NSDictionary){
         let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
  
        dictInfo.setValue(string(dictInfo, "first_name"), forKey: "first_name")
        dictInfo.setValue(string(dictInfo, "last_name"), forKey: "last_name")
        
       dictInfo.setValue(string(dictInfo, "dob"), forKey: "dob")
       dictInfo.setValue(string(dictInfo, "profile_picture"), forKey: "profile_picture")
        
       PredefinedConstants.appDelegate.dictUserInformations = dictInfo
    }
    
    
}//Class Ends Here ===========================.....Neeleshwari

extension UIViewController{
    //Picker view add and remove
    func removePickerView(bottomView:UIView) {
        
        UIView.animate(
            withDuration: Double(0.2),
            animations: {
                bottomView.backgroundColor = UIColor.white
                //  var height = 100;
                
                bottomView.frame = CGRect(x: 0, y: PredefinedConstants.ScreenHeight, width: PredefinedConstants.ScreenWidth, height: bottomView.frame.size.height)
        },
            completion: { finished in
                if(finished) {
                    // self.removeFromSuperview()
                }
        }
        )
        
    }
    
    func addPickerView(bottomView:UIView) {
        
        UIView.animate(
            withDuration: Double(0.2),
            animations: {
                bottomView.backgroundColor = UIColor.white
                //  var height = 100;
                bottomView.frame = CGRect(x: 0, y: PredefinedConstants.ScreenHeight - bottomView.frame.size.height, width: PredefinedConstants.ScreenWidth, height: bottomView.frame.size.height)
        },
            completion: { finished in
                if(finished) {
                    // self.removeFromSuperview()
                }
        }
        )
    }
}



