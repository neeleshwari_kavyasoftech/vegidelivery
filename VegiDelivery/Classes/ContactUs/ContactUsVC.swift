 //
 //  ContactUsVC.swift
 //  VegiDelivery
 //
 //  Created by Kavya Mac Mini 2 on 13/04/18.
 //  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
 //
 
 import Foundation
 import UIKit
 import JVFloatLabeledTextField
 
 class ContactUsVC: UIViewController , UITextFieldDelegate , UITextViewDelegate {
    
    @IBOutlet var scrlView: UIScrollView!
    @IBOutlet var subView: UIView!
    @IBOutlet var tfName: SkyFloatingLabelTextField!
    @IBOutlet var tfContactNumber: SkyFloatingLabelTextField!
    @IBOutlet var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet var tfTitle: SkyFloatingLabelTextField!
    @IBOutlet var tvQuery: JVFloatLabeledTextView!
    @IBOutlet var btnSubmit: UIButton!
    
    //MARK:- Variables
    
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
        self.addDoneButtonOnKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavVegiDeliveryGreen(className: "CONTACT US")
        self.menuNavigationButton()
        self.registerForKeyboardNotifications()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        
        tfName.delegate = self
        tfContactNumber.delegate = self
        tfEmail.delegate = self
        tfTitle.delegate = self
        tvQuery.delegate = self
        
        btnSubmit.border(UIColor.clear, 22, 0)
        
        let userIconName = UIImage(named: "asuser")
        setPaddingWithRightImage(image: userIconName!, textField: tfName)
        
        let userIconMobile = UIImage(named: "mobile")
        setPaddingWithRightImage(image: userIconMobile!, textField: tfContactNumber)
        
        let userIconEmail = UIImage(named: "email")
        setPaddingWithRightImage(image: userIconEmail!, textField: tfEmail)
        
        let userIconTitle = UIImage(named: "title")
        setPaddingWithRightImage(image: userIconTitle!, textField: tfTitle)
        
        
        // let userIconCity = UIImage(named: "email_id")
        //   setPaddingWithRightImage(image: userIconCity!, textField: tfEmail)
        
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "menu_white"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
    }
    
    //MARK:- Button Actions
    @IBAction func btnSubmitAction(_ sender: Any) {
        self.hideKeyboard()
        if let str = checkValidation() {
            Http.alert("", str)
        } else {
            //  self.ws_SignIn()
            self.WS_ContactUs()
        }
    }
    
    //MARK:- Check Validations
    func checkValidation() -> String? {
        
        if tfName.text?.count == 0  {
            return AlertMSG.blankName
            
        }else if  tfContactNumber.text?.count == 0  {
            return AlertMSG.blankContactNumber
            
        } else if (tfContactNumber.text?.count)! < 10 {
            return AlertMSG.invalidContactNumber
            
        } else if tfEmail.text?.count == 0 {
            return AlertMSG.blankEmail
            
        }else if  !Validate.email(tfEmail.text!) {
            return AlertMSG.invalidEmail
            
        } else if (tfTitle.text?.count)! == 0 {
            return AlertMSG.blankTitle
            
        }else if (tvQuery.text?.count)! == 0 {
            return AlertMSG.blankQuery
        }
        
        return nil
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
        scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        scrlView.contentInset = UIEdgeInsets.zero
    }
    
    //MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        /*
         if UIScreen.main.bounds.size.height < 667{
         if (textField.frame.origin.y >= 70) {
         scrlView.setContentOffset(CGPoint(x: 0, y: CGFloat(Int(textField.frame.origin.y - 50))) , animated: true)
         }
         }
         */
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfName {
            tfContactNumber.becomeFirstResponder()
        }else if textField == tfContactNumber {
            tfEmail.becomeFirstResponder()
        }else if textField == tfEmail {
            tfTitle.becomeFirstResponder()
        } else if textField == tfTitle {
            tvQuery.becomeFirstResponder()
        }else if textField == tvQuery {
            tvQuery.resignFirstResponder()
            scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.count)! + string.count - range.length
        if textField == tfName {
            return (length > 40) ? false : true
        }else if textField == tfContactNumber {
            return (length > 10) ? false : true
        }
        else if textField == tfEmail {
            return (length > 20) ? false : true
        }else if textField == tfTitle {
            return (length > 20) ? false : true
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrlView.contentSize = CGSize(width: view.frame.size.width, height: btnSubmit.frame.origin.y + btnSubmit.frame.size.height + 10)
    }
    
    //MARK:- TEXTVIEW_DELEGATE
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        if UIScreen.main.bounds.size.height < 667{
            if (textView.frame.origin.y >= 180) {
                scrlView.setContentOffset(CGPoint(x: 0, y: CGFloat(Int(textView.frame.origin.y - 220))) , animated: true)
            }
        }
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        var maxCharacter = Int()
        maxCharacter = 250
        
        if (text == "\n") {
            tvQuery.resignFirstResponder()
            self.hideKeyboard()
            
            scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            //  scrlView.contentSize = CGSize(width: view.frame.size.width, height: btnSubmit.frame.origin.y + btnSubmit.frame.size.height + 10)
            
            //            if textView.text.count == 0{
            //                textView.title
            //            }
            //
            
            return false
        }
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return (numberOfChars < maxCharacter)
    }
    
    
    ///MARK:- AddDoneButtonOnKeyboard
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        doneToolbar.barTintColor = UIColor(named:"appTheme") //appColor.appThemeGreenColor
        doneToolbar.tintColor = UIColor.white
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let next: UIBarButtonItem = UIBarButtonItem(title: "NEXT", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.nextOfDoneTool))
        let done: UIBarButtonItem = UIBarButtonItem(title: "DONE", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.hideKeyboard))
        
        var items:[UIBarButtonItem] = []
        items.append(next)
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.tfContactNumber.inputAccessoryView = doneToolbar
    }
    
    @objc func nextOfDoneTool() {
        tfEmail.becomeFirstResponder()
    }
    
    func done() {
        tfContactNumber.resignFirstResponder()
        scrlView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    
    //MARK:- KEYBOARD NOTIFICATION METHODS
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scrlView.contentInset = UIEdgeInsets.zero
        } else {
            scrlView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 5, right: 0)
        }
        scrlView.scrollIndicatorInsets = scrlView.contentInset
    }
    //MARK:- WS_ContactUs
    
    func WS_ContactUs() {
        
        let params = NSMutableDictionary()
        params["name"] = tfName.text
        params["email"] = tfEmail.text
        params["subject"] = tfTitle.text
        
        params["message"] = tvQuery.text
        params["contact_number"] = tfContactNumber.text
        params["user_type"] = "customer"
        
        Http.instance().json(WebServices().WS_ContactUs_Support, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    // self.arrOrderList = NSMutableArray()
                    Http.alert("", string(json! , "message"))
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    
 }//Class Ends Here ===========================.....Neeleshwari
 

