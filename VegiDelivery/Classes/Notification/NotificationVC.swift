//
//  NotificationVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 16/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit

class NotificationVC: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet var tblView: UITableView!
    
    //MARK:- Variables
    var arrNotificationList = NSMutableArray()
    var boolWsPage = Bool()
    var page = Int()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavVegiDeliveryGreen(className: "NOTIFICATION")
        self.menuNavigationButton()
        
        page = 1
        self.WS_CustomerNotificationList()
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        tblView.delegate = self
        tblView.dataSource = self
        
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "menu_white"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
        // _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Fuctions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrNotificationList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tblView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        
        let dictData = arrNotificationList.object(at: indexPath.row) as! NSDictionary
        cell.activityIndicator.isHidden = false
        cell.lblTitle.text = string(dictData, "title")
        cell.lblDate.text = string(dictData, "created_at")
        cell.lblDetails.text = string(dictData, "message")
        let imgURL = string(dictData, "filename")
        cell.imgStatus.image = UIImage(url: URL(string: imgURL))
        DispatchQueue.main.asyncAfter(deadline: .now() + 1 ){
            cell.activityIndicator.isHidden = true
        }
        
        ///================
        cell.imgNotification.layer.cornerRadius =   cell.imgNotification.frame.size.height/2
        cell.imgNotification.clipsToBounds = true
        cell.imgNotification.layer.borderColor = UIColor.red.cgColor
        cell.imgNotification.layer.borderWidth = 0
    
        if indexPath.row == (arrNotificationList.count - 1) {
            if !boolWsPage {
                boolWsPage = true
                if arrNotificationList.count % 50 == 0 {
                    page += 1
                    self.WS_CustomerNotificationList()
                }
            }
        }
   
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //MARK:- ------------------ ws_MyOrder List ----------------------//
    func WS_CustomerNotificationList() {
        
        let params = NSMutableDictionary()
        params["page"] = page
        Http.instance().json(WebServices().WS_CustomerNotificationList, params, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    self.arrNotificationList = NSMutableArray()
                    if let arr = json?.object(forKey: "result") as? NSArray {
                            // self.arrList  = arr.mutableCopy() as! NSMutableArray
                            print("arr>>>\(arr)")
                            if self.boolWsPage {
                                self.arrNotificationList.addObjects(from: arr as [AnyObject])
                                self.boolWsPage = false
                            } else {
                                self.arrNotificationList = (arr.mutableCopy() as? NSMutableArray)!
                            }
                            self.tblView.reloadData()
                    }
                    
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    
    
    
}//Class Ends Here ===========================.....Neeleshwari


class NotificationCell :UITableViewCell{
    
    @IBOutlet var viewBG: UIView!
    @IBOutlet var imgNotification: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblDetails: UILabel!
    @IBOutlet var imgStatus: UIImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
}


extension UIImage {
    convenience init?(url: URL?) {
        guard let url = url else { return nil }
        
        do {
            let data = try Data(contentsOf: url)
            self.init(data: data)
        } catch {
            print("Cannot load image from url: \(url) with error: \(error)")
            return nil
        }
    }
}




