//
//  MyOrdersVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 18/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit

class MyOrdersListVC: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet var lblNoRecords: UILabel!
    
    //MARK:- Variables
    var arrOrderList = NSMutableArray()
    var boolWsPage = Bool()
    var page = Int()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavVegiDeliveryGreen(className: "MY ORDERS")
        self.menuNavigationButton()
         lblNoRecords.isHidden = true
        page = 1
        self.ws_MyOrderList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
   
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        tblView.delegate = self
        tblView.dataSource = self
        
        let dummyViewHeight = CGFloat(40)
        self.tblView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.bounds.size.width, height: dummyViewHeight))
        self.tblView.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "menu_white"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
        //_ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Table View delegates and datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrOrderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "MyOrdersListCell") as! MyOrdersListCell
        
        let dictData = arrOrderList.object(at: indexPath.row) as! NSDictionary
        
        cell.lblOrderId.text =  "Order ID: " + string(dictData, "order_id")
        cell.lblTotalPrice.text = "Total Price: ₹" +  string(dictData, "total")
        cell.lblTotalItems.text = "Total Items: " +  string(dictData, "no_of_product")
        cell.lblCOD.text =   string(dictData, "shipping_method")
       let order_status_id = string(dictData, "order_status_id")
       // cell.lblStatus.text = status
        
        if order_status_id == "1" || order_status_id == "0"{
            cell.lblStatus.textColor = appColor.pending
            cell.lblStatus.text = "Pending"
        }else if order_status_id == "2"{
            cell.lblStatus.textColor = appColor.inprocess
            cell.lblStatus.text = "Inprocess"
        }else if order_status_id == "3" {
            cell.lblStatus.textColor = appColor.dispatched
            cell.lblStatus.text = "Dispatched"
        }else if order_status_id == "4"{
            cell.lblStatus.textColor = appColor.completed
            cell.lblStatus.text = "Delivered"
        }else if order_status_id == "5"{
            cell.lblStatus.textColor = appColor.cancel
            cell.lblStatus.text = "Cancelled"
        }else{
            cell.lblStatus.textColor = appColor.pending
            cell.lblStatus.text = "Pending"
        }
        
        cell.imgOrder.layer.cornerRadius =   cell.imgOrder.frame.size.height/2
        cell.imgOrder.clipsToBounds = true
        cell.imgOrder.layer.borderColor = UIColor.red.cgColor
        cell.imgOrder.layer.borderWidth = 0
        
        if indexPath.row == (arrOrderList.count - 1) {
            if !boolWsPage {
                boolWsPage = true
                if arrOrderList.count % 50 == 0 {
                    page += 1
                    self.ws_MyOrderList()
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
        let dictData = arrOrderList.object(at: indexPath.row) as! NSDictionary
        
        var order_id = ""
        order_id =  string(dictData, "order_id")
        let newVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
         newVC.order_id = order_id
         self.navigationController?.pushViewController(newVC, animated: true)
 
    }
    
    //MARK:- ------------------ ws_MyOrder List ----------------------//
    func ws_MyOrderList() {
        
        let params = NSMutableDictionary()
        params["page"] = page
        
        print(tokenClass.getToken())
        
        Http.instance().json(WebServices().WS_Order_List, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            print("json-->>>\(json!)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    self.arrOrderList = NSMutableArray()
                     if let result = json?.object(forKey: "result") as? NSDictionary {
                    if let arr = result.object(forKey: "order_list") as? NSArray {
                        // self.arrList  = arr.mutableCopy() as! NSMutableArray
                        print("arr>>>\(arr)")
                       
                        if self.boolWsPage {
                            self.arrOrderList.addObjects(from: arr as [AnyObject])
                            self.boolWsPage = false
                        } else {
                            self.arrOrderList = (arr.mutableCopy() as? NSMutableArray)!
                        }
                        if self.arrOrderList.count == 0 {
                            self.lblNoRecords.isHidden  = false
                        }else{
                            self.lblNoRecords.isHidden  = true
                        }
                         self.tblView.reloadData()
                        }
                    }
                   
                } else {
                   // Http.alert("", string(json! , "message"))
                    self.lblNoRecords.isHidden  = false
                    self.lblNoRecords.text = string(json! , "message")
                    self.arrOrderList = NSMutableArray()
                    self.tblView.reloadData()
                }
            }
        }
    }
    
}//Class Ends Here ===========================.....Neeleshwari

class MyOrdersListCell :UITableViewCell{
    
    @IBOutlet var viewBG: View!
    @IBOutlet var imgOrder: UIImageView!
    
    @IBOutlet var lblOrderId: UILabel!
    @IBOutlet var lblTotalPrice: UILabel!
    @IBOutlet var lblTotalItems: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblCOD: UILabel!
    
    //Variables
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}




