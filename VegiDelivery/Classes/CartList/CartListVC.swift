//
//  CartListVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 18/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit

class CartListVC: UIViewController , UITableViewDelegate , UITableViewDataSource , UIGestureRecognizerDelegate , CartListCellDelegate {
    
    @IBOutlet var tblView: UITableView!
    //Pop Up view
    @IBOutlet var viewMain: UIView!
    @IBOutlet var viewPopUP: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblMsg: UILabel!
    @IBOutlet var btnYes: UIButton!
    @IBOutlet var btnNo: UIButton!
    @IBOutlet var lblNoRecord: UILabel!
    
    @IBOutlet var lblTotalAmount: UILabel!
    @IBOutlet var lblStaticAmountPayable: UILabel!
    @IBOutlet var viewCheckOut: UIView!
    @IBOutlet var btnContinueShopping: UIButton!
    @IBOutlet var btnCheckOut: UIButton!
    
    //MARK:- Variables
    var arrCartList = NSMutableArray()
    var boolWsPage = Bool()
    var page = Int()
    var lastClass = ""
    var intIndexSelected = -1
    var floatTotalPrice = Float()
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
        
        // tblView.isScrollEnabled = false
        // var count = arrCartList.count
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavVegiDeliveryGreen(className: "CART")
        self.lastClassBack()
        setWhiteBackButton()
        lblNoRecord.isHidden = true
        page = 1
        self.WS_CartList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    func lastClassBack()  {
        
        if lastClass == "product"{
            self.backNavigationButton()
        }else{
            self.menuNavigationButton()
        }
    }
    func backNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "ic_arrow_back"), style: .plain, target: self, action: #selector(actionBackButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionBackButton()  {
        //self.sideMenuViewController.presentLeftMenuViewController()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        tblView.delegate = self
        tblView.dataSource = self
        
        let dummyViewHeight = CGFloat(40)
        self.tblView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.bounds.size.width, height: dummyViewHeight))
        self.tblView.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
        // arrCartList = ["Apple","Onion", "grapes","mango"]
        
        btnContinueShopping.border(UIColor(named: "redColor"), 20, 1)
        btnCheckOut.border(UIColor(named: "redColor"), 20, 1)
        btnYes.border(UIColor(named: "redColor"), 20, 1)
        btnNo.border(UIColor(named: "redColor"), 20, 1)
        
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "menu_white"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
        // _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Button Actions
    
    @IBAction func btnContinueShoppingAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btnCheckOutAction(_ sender: Any) {
        
        let dictInfo = PredefinedConstants.appDelegate.dictUserInformations
        
        if string(dictInfo, "first_name").count != 0 {
            PredefinedConstants.appDelegate.arrCartList = arrCartList
            PredefinedConstants.appDelegate.floatTotalPrice = floatTotalPrice
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutDetailsVC") as! CheckOutDetailsVC
            vc.arrItemList = arrCartList
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            Http.alert("", "Please complete your profile")
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            vc.lastClass = "cart"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //Pop UP View button actions
    @IBAction func btnYesAction(_ sender: Any) {
       removeSubViewWithAnimation(viewMain: viewMain, viewPopUP: viewPopUP)
        self.WS_DELETE_CART_ITEM()
    }
    
    @IBAction func btnNoAction(_ sender: Any) {
        
        removeSubViewWithAnimation(viewMain: viewMain, viewPopUP: viewPopUP)
    }
    
    //MARK:- Table View delegates and datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrCartList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "CartListCell") as! CartListCell
        
        let dict = arrCartList.object(at: indexPath.row) as! NSDictionary
        
        cell.lblName.text = string(dict, "title")
        cell.lblPrice.text = "₹\(string(dict, "price"))"//"Price: ₹4545.20"
        
        cell.lblTotalItems.text = string(dict, "quantity")
        let strOption = string(dict, "option")
        let dictOption = convertToDictionary(text: strOption)
        cell.lblWeight.text = string(dict, "weight")
        
        //product image
        let UserImage_URLString = WebServices().WEB_URL
        let strUserImg = string(dict, "image")
        let imageUrl = UserImage_URLString + strUserImg
        
        if strUserImg.isEmpty {
            cell.imgProduct.image = UIImage(named: "vegi_default")
        } else {
            let url =  NSURL(string: imageUrl)
            // cell.imgProduct.af_setImage(withURL: url as URL!)
            cell.imgProduct.sd_setImage(with: url as URL?, placeholderImage: UIImage(named: imageUrl), options: SDWebImageOptions.retryFailed)
        }
        
        cell.delegateCartListCell = self
        cell.intIndex = indexPath.row
        
        if indexPath.row == (arrCartList.count - 1) {
            if !boolWsPage {
                boolWsPage = true
                if arrCartList.count % 50 == 0 {
                    print("indexPath.row:\(indexPath.row)")
                    page += 1
                    self.WS_CartList()
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //MARK:- cell delegates
    func btnPlus(intIndex: Int){
        print("btnPlus intIndex:\(intIndex)")
        let dict = arrCartList.object(at: intIndex) as! NSDictionary
        
        let quantity = string(dict, "quantity")
        var intQuantity = Int(quantity)!
        print("intQuantity>>>\(intQuantity)")
        intQuantity += 1
        let cart_id = string(dict, "cart_id")
        
        WS_EDIT_CART_ITEM(quantity: intQuantity, cart_id: cart_id)
    }
    
    func btnMinus(intIndex: Int){
        let dict = arrCartList.object(at: intIndex) as! NSDictionary
        let cart_id = string(dict, "cart_id")
        
        let quantity = string(dict, "quantity")
        var intQuantity = Int(quantity)!
        print("intQuantity>>>\(intQuantity)")
        
        if intQuantity == 1 {
            WS_DELETE_CART_ITEM()
        }else{
            
            intQuantity -= 1
            WS_EDIT_CART_ITEM(quantity: intQuantity, cart_id: cart_id)
        }
    }
    
    func btnDelete(intIndex: Int){
        intIndexSelected = intIndex
        self.addPopupShowConfirmation(viewMain: viewMain, viewPopUP: viewPopUP)
    }
    
    ////  ----------------------------------------
    
    //MARK:- FUNCTION FOR POPVIEW
    func addPopupShowConfirmation(viewMain: UIView, viewPopUP: UIView)  {
        viewMain.frame = self.view.frame
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        // self.view.window?.addSubview(viewMain)
        self.view.addSubview(viewMain)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = (self as UIGestureRecognizerDelegate)
        viewMain.addGestureRecognizer(tap)
        
        viewPopUP.leftAnchor.constraint(equalTo: viewPopUP.leftAnchor, constant: 15).isActive = true
        viewPopUP.rightAnchor.constraint(equalTo: viewPopUP.rightAnchor, constant: -15).isActive = true
        
        viewMain.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        viewMain.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        self.test(viewTest: viewPopUP)
    }
    
    func test(viewTest: UIView) {
        let orignalT: CGAffineTransform = viewTest.transform
        viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.4, animations: {
            
            viewTest.transform = orignalT
        }, completion:nil)
    }
    
    func removeSubViewWithAnimation(viewMain: UIView, viewPopUP: UIView) {
        let orignalT: CGAffineTransform = viewPopUP.transform
        UIView.animate(withDuration: 0.3, animations: {
            viewPopUP.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        }, completion: {(sucess) in
            viewMain.removeFromSuperview()
            viewPopUP.transform = orignalT
        })
        
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        //viewPopup.removeFromSuperview()
        removeSubViewWithAnimation(viewMain: viewMain, viewPopUP: viewPopUP)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if viewMain.bounds.contains(touch.location(in: viewPopUP)) {
            return false
        }
        
        return true
    }
    
    //MARK:- ------------------ cart List ----------------------//
    func WS_CartList() {
        
        let params = NSMutableDictionary()
        
        Http.instance().json(WebServices().WS_CartList, params, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            print("json-->>>\(json!)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    self.arrCartList = NSMutableArray()
                    if let arr = json?.object(forKey: "cart_item") as? NSArray {
                        // self.arrList  = arr.mutableCopy() as! NSMutableArray
                        print("arr>>>\(arr)")
                        
                        if self.boolWsPage {
                            self.arrCartList.addObjects(from: arr as [AnyObject])
                            self.boolWsPage = false
                        } else {
                            self.arrCartList = (arr.mutableCopy() as? NSMutableArray)!
                        }
                        // self.tableViewHt()
                        if self.arrCartList.count == 0 {
                            self.lblNoRecord.isHidden  = false
                        }else{
                            self.lblNoRecord.isHidden  = true
                        }
                        self.tblView.reloadData()
                        self.totalAmount()
                    }
                } else {
                    // Http.alert("", string(json! , "message"))
                    self.lblNoRecord.isHidden  = false
                    self.lblNoRecord.text = string(json! , "message")
                    self.arrCartList = NSMutableArray()
                    self.tblView.reloadData()
                    self.floatTotalPrice = 0.00
                }
            }
        }
    }
    
    //WS_DELETE_CART_ITEM
    func WS_DELETE_CART_ITEM() {
        
        let dict =   arrCartList.object(at:intIndexSelected) as! NSDictionary
        let cart_id = string(dict, "cart_id")
        
        let params = NSMutableDictionary()
        params["cart_id"] = cart_id
        
        Http.instance().json(WebServices().WS_DELETE_CART_ITEM, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            print("json-->>>\(json!)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    Http.alert("", string(json! , "message"))
                    self.WS_CartList()
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    //WS_EDIT_CART_ITEM
    func WS_EDIT_CART_ITEM(quantity:Int,cart_id:String) {
        
        let params = NSMutableDictionary()
        params["cart_id"] = cart_id
        params["cart_id"] = cart_id
        
        Http.instance().json(WebServices().WS_EDIT_CART_ITEM, params, "POST", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
            print("json-->>>\(json!)")
            
            if json != nil {
                let json = json as? NSDictionary
                if number(json! , "success").boolValue {
                    Http.alert("", string(json! , "message"))
                    self.WS_CartList()
                } else {
                    Http.alert("", string(json! , "message"))
                }
            }
        }
    }
    
    
    
    func totalAmount(){
        self.floatTotalPrice = 0.00
        
        for i in 0..<arrCartList.count {
            
            let dict = arrCartList.object(at: i) as! NSDictionary
            
            let amt = Float(number(dict, "price"))
            print("amt>>>\(amt)")
            floatTotalPrice += amt
        }
        lblTotalAmount.text = "₹\(floatTotalPrice)"///
        print("floatTotalPrice>>>\(floatTotalPrice)")
        PredefinedConstants.appDelegate.floatTotalPrice = floatTotalPrice
    }
    
    
}//Class Ends Here ===========================.....Neeleshwari


//MARK:- CartListCell

public protocol CartListCellDelegate{
    func btnPlus(intIndex: Int)
    func btnMinus(intIndex: Int)
    func btnDelete(intIndex: Int)
}

class CartListCell :UITableViewCell{
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblWeight: UILabel!
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var btnMinus: UIButton!
    @IBOutlet var btnPlus: UIButton!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var lblTotalItems: UILabel!
    
    //Variables
    
    var delegateCartListCell:CartListCellDelegate?
    var intIndex = Int()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func btnDeleteAction(_ sender: Any) {
        delegateCartListCell?.btnDelete(intIndex: intIndex)
        
    }
    
    @IBAction func btnMinus(_ sender: Any) {
        delegateCartListCell?.btnMinus(intIndex: intIndex)
        
    }
    
    @IBAction func btnPlusAction(_ sender: Any) {
        delegateCartListCell?.btnPlus(intIndex: intIndex)
    }
    
}
////////////////////////////===================


func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}



