
//
//  HomeVC.swift
//  VegiDelivery
//
//  Created by Kavya Mac Mini 2 on 25/04/18.
//  Copyright © 2018 Kavya Mac Mini 2. All rights reserved.
//

import Foundation
import UIKit

class HomeVC: UIViewController , AACarouselDelegate{
    
    @IBOutlet var carouselView: AACarousel!
    @IBOutlet var viewVegetables: UIView!
    @IBOutlet var viewFruits: UIView!
    @IBOutlet var btnVegetable: UIButton!
    @IBOutlet var btnFruits: UIButton!
    
    //MARK:- Variables
    var arrCategoryList = NSMutableArray()
    
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAndDelegates()
        self.initializeImageFlipper()
        // self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //setNavVegiDeliveryGreen(className: "VEGI DELIVERY")
        self.menuNavigationButton()
        self.setNavShopping(className: "VEGI DELIVERY")
        
        // self.WS_CategoryList()
    }
    
    func getDataForCart() {
        
        
    }
    
    //MARK:- Fuctions
    func displayAndDelegates()  {
        
        
        
    }
    
    func menuNavigationButton()  {
        let button1 = UIBarButtonItem(image: UIImage(named: "menu_white"), style: .plain, target: self, action: #selector(actionMenuButton)) //
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc func actionMenuButton()  {
        self.sideMenuViewController.presentLeftMenuViewController()
        // _ = self.navigationController?.popViewController(animated: true)
    }
    
    func setNavShopping(className:String)  {
        
        let btnNavCart = UIButton(type: .custom)
        let lblTotalCount = UILabel()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "appTheme")
        
        let titleViewNav = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 60, height: 40))
        self.navigationItem.titleView = titleViewNav
        
        let lbl1 = UILabel(frame: CGRect.init(x: 0 , y: 0, width: 50, height: 40))
        lbl1.text = className
        lbl1.font = UIFont.boldSystemFont(ofSize: 18.0)
        lbl1.textColor = UIColor.white
        lbl1.numberOfLines = 1
        lbl1.sizeToFit()
        lbl1.center.y = titleViewNav.center.y
        if lbl1.frame.size.width > self.view.frame.size.width - 170 {
            lbl1.frame.size.width = self.view.frame.size.width - 170
        }
        
        titleViewNav.addSubview(lbl1)
        
        //////////////
        
        let imgN = imageWithImage(image: UIImage(named: "cart_menu")!, scaledToSize: CGSize(width: 25.0, height: 25.0))
        
        btnNavCart.setImage(imgN, for: .normal)
        btnNavCart.frame = CGRect(x: titleViewNav.frame.size.width - 50, y: 5, width: 30, height: 30)
        btnNavCart.contentMode = .scaleAspectFit
        btnNavCart.addTarget(self, action: #selector(actionCart), for: .touchUpInside)
        titleViewNav.addSubview(btnNavCart)
        btnNavCart.backgroundColor = UIColor.clear
        
        lblTotalCount.frame =  CGRect.init(x: btnNavCart.frame.origin.x + 25 , y: 0, width: 20 , height: 20)
        
        let cartCount = PredefinedConstants.appDelegate.intCartCount
        if cartCount == 0 {
            lblTotalCount.isHidden = true
        }else{
            lblTotalCount.isHidden = false
        }
        
        lblTotalCount.text = "\(cartCount)"//"15"
        lblTotalCount.font = UIFont.systemFont(ofSize: 10.0)
        lblTotalCount.textColor = UIColor.white
        lblTotalCount.numberOfLines = 1
        lblTotalCount.backgroundColor = UIColor(named: "redColor")
        
        lblTotalCount.textAlignment = .center
        titleViewNav.addSubview(lblTotalCount)
        lblTotalCount.layer.cornerRadius = lblTotalCount.frame.size.width/2
        lblTotalCount.clipsToBounds = true
    }
    
    @objc func actionCart()  {
        print("actionCartCount")
        
        let newVC = self.storyboard?.instantiateViewController(withIdentifier: "CartListVC") as! CartListVC
        newVC.lastClass = "product"
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    //Mark:- go to Cart View
    /*
     func addCartButton() {
     let buttonCart = UIButton.init(type: .custom)
     buttonCart.setImage(UIImage.init(named: "cart_menu"), for: UIControlState.normal)
     buttonCart.addTarget(self, action:#selector(btnCartAction), for:.touchUpInside)
     buttonCart.frame = CGRect.init(x: 0, y: 0, width: 15, height: 15)
     let barButton = UIBarButtonItem.init(customView: buttonCart)
     self.navigationItem.rightBarButtonItem = barButton
     }
     @objc func btnCartAction() {
     print("button press message btn")
     let vc = self.storyboard?.instantiateViewController(withIdentifier: "CartListVC") as! CartListVC
     self.navigationController?.pushViewController(vc, animated: true)
     }
     */
    
    func setCategoryData() {
        print("arr>>>\(arrCategoryList)")
        if arrCategoryList.count != 0{
            let dict1 = arrCategoryList.object(at: 0) as! NSDictionary
            let dict2 = arrCategoryList.object(at: 1) as! NSDictionary
            print("dict1>>\(dict1)")
            print("dict2>>\(dict2)")
            
            btnVegetable.setTitle(string(dict1, "name").uppercased(), for: .normal)
            btnFruits.setTitle(string(dict2, "name").uppercased(), for: .normal)
        }
        
    }
    
    /*
     
     if arrCategoryList.count != 0{
     let dict1 = arrCategoryList.object(at: 0) as! NSDictionary
     let strTitle = string(dict1, "name").uppercased()
     let category_id = string(dict1, "category_id")
     
     let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
     vc.strTitle = strTitle
     vc.category_id = category_id
     self.navigationController?.pushViewController(vc, animated: true)
     
     }
     if arrCategoryList.count != 0{
     let dict2 = arrCategoryList.object(at: 1) as! NSDictionary
     let strTitle = string(dict2, "name").uppercased()
     let category_id = string(dict2, "category_id")
     
     
     let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
     vc.strTitle = strTitle
     vc.category_id = category_id
     self.navigationController?.pushViewController(vc, animated: true)
     }
     */
    
    
    //MARK:- Button Actions
    
    @IBAction func btnVegetableAction(_ sender: Any) {
        print("btnVegetableAction")
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
        vc.strTitle = "Vegetables".uppercased()
        vc.category_id = "1"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btnFruitsAction(_ sender: Any) {
        print("btnFruitsAction")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
        vc.strTitle = "Fruits"
        vc.category_id = "2"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    ///Neeleshwari====================================================================================
    //MARK: Carousal View.---------
    
    func initializeImageFlipper() {
        
        let pathArray = ["home_img_1","home_img_2","home_img_3"]
        
        // addShadow(toView: carouselView, cornerRadius: 5)
        carouselView.delegate = self
        carouselView.setCarouselData(paths: pathArray,  describedTitle: [""], isAutoScroll: true, timer: 3.0, defaultImage: "default_vegi_green")
        carouselView.setCarouselOpaque(layer: false, describedTitle: false, pageIndicator: false)
        // carouselView.setCarouselLayout(displayStyle: 0, pageIndicatorPositon: 2, pageIndicatorColor: nil, describedTitleColor: nil, layerColor: UIColor.clear)
        
        carouselView.setCarouselLayout(displayStyle: 0, pageIndicatorPositon: 2, pageIndicatorColor: nil, describedTitleColor: nil, layerColor: UIColor.black.withAlphaComponent(0.30))
        
        
    }
    
    func addShadow(toView: UIView, cornerRadius: CGFloat) {
        toView.layer.shadowColor = UIColor.black.cgColor
        toView.layer.shadowOpacity = 0.8
        toView.layer.shadowOffset = CGSize.zero
        toView.layer.shadowRadius = 10
        toView.layer.shadowPath = UIBezierPath(rect: carouselView.bounds).cgPath
        toView.clipsToBounds = true
        toView.layer.cornerRadius = cornerRadius
    }
    
    func downloadImages(_ url: String, _ index:Int) {
        /*
         let imageView = UIImageView()
         
         if url != "" {
         imageView.kf.setImage(with: URL(string: url)!, placeholder: UIImage.init(named: "defaultImage"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { (downloadImage, error, cacheType, url) in
         if downloadImage != nil {
         self.carouselView.images[index] = downloadImage!
         }
         })
         
         }
         */
    }
    
    //optional method (interaction for touch image)
    func didSelectCarouselView(_ view:AACarousel ,_ index:Int) {
        print("index -\(index)-")
    }
    
    //optional method (show first image faster during downloading of all images)
    func callBackFirstDisplayView(_ imageView: UIImageView, _ url: [String], _ index: Int) {
        //  imageView.kf.setImage(with: URL(string: url[index]), placeholder: UIImage.init(named: "defaultImage"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
    }
    
    func startAutoScroll() {
        //optional method
        carouselView.startScrollImageView()
    }
    
    func stopAutoScroll() {
        //optional method
        carouselView.stopScrollImageView()
    }
    
    
    //MARK:- ------------------ category List -----------------
    /*
     func WS_CategoryList() {
     let params = NSMutableDictionary()
     //        params["category_id"] = "1"
     //        params["search_value"] = ""
     //        params["user_id"] = "48"
     params["page"] = "1"
     
     Http.instance().json(WebServices().WS_CATEGORY_LIST, params, "GET", ai: true, popup: true, prnt: true, tokenClass.getToken()) { (json, params)  in
     print("json-->>>\(json!)")
     
     if json != nil {
     let json = json as? NSDictionary
     if number(json! , "success").boolValue {
     //  self.arrProductList = NSMutableArray()
     if let result = json?.object(forKey: "result") as?  NSDictionary {
     
     if let arr = result.object(forKey: "category") as?  NSArray {
     self.arrCategoryList  = arr.mutableCopy() as! NSMutableArray
     print("arr>>>\(arr)")
     self.setCategoryData()
     }
     }
     
     } else {
     Http.alert("", string(json! , "message"))
     }
     }
     }
     }
     */
    
}//Class Ends Here ===========================.....Neeleshwari


